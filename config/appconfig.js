/**
 * @file config appconfig.js
 * @author suwandi aminsa sangaji
 */

require("dotenv").config();

module.exports = {
  app: {
    port: process.env.DEV_APP_PORT || 8000,
    appName: process.env.APP_NAME || "CarApi",
    env: process.env.NODE_ENV || "development",
    server: process.env.SERVER || "localhost",
  }
};