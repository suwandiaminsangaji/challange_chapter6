/**
 * @file utils datetime.js
 * @author suwandi aminsa sangaji
 */

const datetime = function () {
  let date = new Date();
  return (
    date.getFullYear().toString() +
    date.getMonth().toString() +
    date.getDay().toString() +
    date.getHours().toString() +
    date.getMinutes().toString() +
    date.getSeconds().toString()
  );
};

module.exports = datetime;