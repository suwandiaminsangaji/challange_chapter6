const path = require("path");
const fs = require("fs");
const Resizer = require("../../utils/Resizer");
const carRepository = require('../repositories/carRepository');

class carService {
  /**
   * @function getList carService.js
   * @author suwandi aminsa sangajiu
   */
  static async getList(filter, offset = 0, limit = 15) {
    try {
      let whereClause = new Object({
        where: {
          deleted: false
        },
        offset: offset,
        limit: limit
      });
      if (filter) {
        whereClause.where.size_id = filter;
        console.log(whereClause);
      }
      let { count, rows } = await carRepository.findAllPartially(whereClause);
      
      return {
        data: rows,
        total: count,
        offset: offset,
        limit: limit
      };
    } catch (error) {
      throw error;
    }
  }

  static get(id) {
    try {
      // Tulis kode logika anda disini

    } catch (error) {
      throw error;
    }
  }

  static create(dataObj) {
    try {
      // Tulis kode logika anda disini
      
    } catch (error) {
      throw error;
    }
  }

  static update(dataObj) {
    try {
      // Tulis kode logika anda disini
      
    } catch (error) {
      throw error;
    }
  }

  static delete(dataObj) {
    try {
      // Tulis kode logika anda disini
      
    } catch (error) {
      throw error;
    }
  }
}

module.exports = carService;