const carService = require('../../services/carService');

class carController {
  /**
   * @function getAll carController.js
   * @author suwandi aminsa sangaji
   */
  static getAll(req, res) {
    let { filter, offset, limit } = req.params;
    carService.getList(filter, offset, limit)
      .then(( { data, total, offset, limit } ) => {
        if (!total)
          res.status(404).json({ code: 404, message: "No Data" });
        else
          res.status(200).json({
            code: 200, 
            message: "OK", 
            data: { 
              cars: data
            },
            meta: {
              total: total,
              offset: offset,
              limit: limit
            }
          });
      })
      .catch((error) => {
        console.error(error);
        res.status(500).json({
          code: 500,
          message: "Internal Server Error"
        });
      });
  }

  static getCarByID(req, res) {
    // Tulis kode logika controller dengan contoh seperti diatas (pakai promise)
  }

  static add(req, res) {
    // Tulis kode logika controller dengan contoh seperti diatas (pakai promise)
  }

  static edit(req, res) {
    // Tulis kode logika controller dengan contoh seperti diatas (pakai promise)
  }

  static delete(req, res) {
    // Tulis kode logika controller dengan contoh seperti diatas (pakai promise)
  }
}

module.exports = carController;