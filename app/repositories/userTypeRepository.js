const { UserType } = require('../models');

/**
 * @class userTypeRepository userTypeRepository.js
 * @author suwandi aminsa sangaji
 */
class userTypeRepository {
  static findAll() {
    return UserType.findAll();
  }
  static find(id) {
    return UserType.findByPk(id);
  }
}

module.exports = userTypeRepository;