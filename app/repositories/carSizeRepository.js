const { CarSize } = require('../models');

/**
 * @class carSizeRepository carSizeRepository.js
 * @author suwandi aminsa sangaji
 */
class carSizeRepository {
  static findAll() {
    return CarSize.findAll();
  }
  static find(id) {
    return CarSize.findByPk(id);
  }
}

module.exports = carSizeRepository;