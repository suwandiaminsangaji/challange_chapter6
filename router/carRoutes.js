/**
 * @file router carRoutes.js
 * @author suwandi aminsa sangaji
 */

const router = require('express').Router();
const upload = require('../utils/uploadMiddleware');
const carController = require('../app/controllers/api/carController');

// REST API ROUTES

/**
 * @swagger
 * /api/car/getAll:
 *   get:
 *     summary: get All car data or partially
 *     description: Retrieve car data from database
 *     parameters:
 *       - in: path
 *         name: filter
 *         required: false
 *         description: Numeric ID of the carsize id to retrieve specified car data
 *         schema:
 *           type: integer
 *       - in: path
 *         name: offset
 *         required: false
 *         description: Numeric value to skip some car data
 *         schema:
 *           type: integer
 *       - in: path
 *         name: limit
 *         required: false
 *         description: Numeric value to limit retrieved car data
 *         schema:
 *           type: integer
 *     responses:
 *       '200':
 *         description: Get All data or partially from Car
 *       '404':
 *         description: No Data
 *       '500':
 *         description: Internal Server Error
 */
router.get("/getAll", carController.getAll);

/**
 * HTTP GET "/api/car/getById"
 * return json
 */
router.get("/getById", carController.getCarByID);

/**
 * HTTP POST "/api/car/add"
 * return json
 */
router.post("/add", upload.single("photo"), carController.add);

/**
 * HTTP PUT "/api/car/edit"
 * return json
 */
router.put("/edit", upload.single("photo"), carController.edit);

/**
 * HTTP DELETE "/api/car/delete"
 * return json
 */
router.delete("/delete", carController.delete);

module.exports = router;
